import React from 'react';
// @ts-ignore
import _ from 'lodash';
// @ts-ignore
import shallowCompare from 'react-addons-shallow-compare';
import { setPropValue } from 'react-updaters';

interface Iprops {
  tabs: {} | [];
  value: string | number;
  onChange(e: React.SyntheticEvent): void;
}

/** A set of Bootstrap tabs that you can toggle between for navigation */
export default class Tabs extends React.Component<Iprops> {
  shouldComponentUpdate(nextProps: Iprops, nextState: any) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { value: activeTab } = this.props;
    let { tabs } = this.props;

    if (!(tabs instanceof Array)) {
      tabs = _.map(tabs, (label: string, value: number) => ({ label, value }));
    }

    return (
      <ul className="nav nav-tabs" role="navigation">
        {(tabs as []).map(
          ({
            label,
            value: tab,
            liClassName,
            aClassName,
          }: {
            label: string;
            value: number;
            liClassName: string;
            aClassName: string;
          }) => (
            <li
              key={tab}
              className={
                (activeTab === tab ? 'active ' : '') + (liClassName || '')
              }
            >
              <a
                href={`#openTab${tab}`}
                className={aClassName}
                onClick={setPropValue(this, 'onChange', 'tab', null, tab, true)}
              >
                {label}
              </a>
            </li>
          ),
        )}
      </ul>
    );
  }
}
